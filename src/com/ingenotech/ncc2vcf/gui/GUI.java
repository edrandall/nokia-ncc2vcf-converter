/*
 * $Id$
 * Created on 4 Apr 2008
 */

package com.ingenotech.ncc2vcf.gui;

import javax.swing.*;

import com.ingenotech.ncc2vcf.Convert;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.concurrent.TimeUnit;

public class GUI {
	JFrame	mainwin;
	
	public GUI() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				initUI();
			}
		});
	}
	
	void initUI() {
		
		JPanel p = new JPanel(new SpringLayout());
		
		JLabel nccLabel = new JLabel(".ncc file:");
		p.add(nccLabel);

		JTextField nccText = new JTextField(40);
		p.add(nccText);

		JButton nccBrowse = new JButton();
		ChooseFileAction nccChooser = new ChooseFileAction("Browse...", 
                							     			nccText,
                							     			JFileChooser.FILES_ONLY,
                							     			new File("."),
                							     			new File("PhoneBook.ncc"));
		nccBrowse.setAction(nccChooser);
		p.add(nccBrowse);
		
		JLabel vcfLabel = new JLabel(".vcf dir:");
		p.add(vcfLabel);

		JTextField vcfText = new JTextField(40);
		p.add(vcfText);

		JButton vcfBrowse = new JButton();
		ChooseFileAction vcfChooser = new ChooseFileAction("Browse...", 
                										   vcfText,
                										   JFileChooser.DIRECTORIES_ONLY,
                										   new File("."),
                										   new File("vcf"));
		vcfBrowse.setAction(vcfChooser);
		p.add(vcfBrowse);
		
		SpringUtilities.makeCompactGrid(p,
										2, 3, 		//rows, cols
										6, 6,       //initX, initY
										6, 6);      //xPad, yPad
		
		JPanel bp = new JPanel( new GridLayout(1, 2, 10, 10));
		JButton okButton = new JButton();
		okButton.setAction(new ConvertAction("Convert", nccChooser, vcfChooser));
		bp.add(okButton);
	
		JButton cancelButton = new JButton();
		cancelButton.setAction(new ExitAction("Exit"));
		bp.add(cancelButton);

		JPanel bpc = new JPanel(new FlowLayout());
		bpc.add(bp);
		
		mainwin = new JFrame("ncc2vcf");
		mainwin.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		
		Container cp = mainwin.getContentPane();
		cp.setLayout(new BorderLayout());
		cp.add(p, BorderLayout.CENTER);
		cp.add(bpc, BorderLayout.SOUTH);
		
		mainwin.pack();
		mainwin.setVisible(true);
	}
	

	
	
	@SuppressWarnings("serial")
	class ChooseFileAction extends AbstractAction {
		private JFileChooser chooser;
		private JTextField	 textField;
		private File		 selected;
		
		public ChooseFileAction(String 		name,
				                JTextField	textField,
				                int         mode,
				                File		cwd,
				                File        defFile) {
			putValue(NAME, name);
			this.selected = defFile; 
			this.chooser = new JFileChooser(cwd);
			this.chooser.setFileSelectionMode(mode);
			this.textField = textField;
			this.textField.setText(this.selected.getAbsolutePath());
		}
		
		public File	getSelected() {
			return this.selected;
		}
		
		public void actionPerformed(ActionEvent e) {
			int ret = this.chooser.showOpenDialog(mainwin);
			if (ret == JFileChooser.APPROVE_OPTION) {
				this.selected = this.chooser.getSelectedFile();
				this.textField.setText(this.selected.getAbsolutePath());
			}
			
		}
	}

	
	@SuppressWarnings("serial")
	class ExitAction extends AbstractAction {
		
		public ExitAction(String label) {
			putValue(NAME, label);
		}
		
		public void actionPerformed(ActionEvent e) {
			System.exit(0);
		}
	}

	
	@SuppressWarnings("serial")
	class ConvertAction extends AbstractAction {
		
		private ChooseFileAction	nccChooser;
		private ChooseFileAction	vcfChooser;
		
		public ConvertAction(String label,
				             ChooseFileAction	nccChooser,
				             ChooseFileAction	vcfChooser ) {
			putValue(NAME, label);
			this.nccChooser = nccChooser;
			this.vcfChooser = vcfChooser;
		}
		
		public void actionPerformed(ActionEvent e) {
			
			ConvertWorker cw = new ConvertWorker(nccChooser.getSelected(),vcfChooser.getSelected());
			cw.execute();
			try {
				String status = cw.get(5, TimeUnit.SECONDS);
				
				final JDialog dialog = new JDialog(mainwin, true);
				dialog.setTitle("Conversion status");
				Container cp = dialog.getContentPane();
				cp.setLayout(new BorderLayout(20,20));
				JLabel label = new JLabel(status);
				cp.add(label, BorderLayout.CENTER);
				JButton ok = new JButton("OK");
				ok.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent ev) {
						dialog.setVisible(false);
					}
				});
				cp.add(ok, BorderLayout.SOUTH);
				dialog.pack();
				dialog.setVisible(true);
				dialog.dispose();

			} catch (Exception ex) {
			}
		}
	}
	
	
	static class ConvertWorker extends SwingWorker<String, Void> {

		private Convert	convert;
		
		public ConvertWorker(File nccFile,
				       	     File vcfDir) {
			this.convert = new Convert(nccFile, vcfDir);
		}
		
		@Override
		protected String doInBackground() throws Exception {
			String msg = null;
			
			convert.run();
			int status = convert.getStatus();
			if (status == Convert.SUCCESS) {
				msg = "Conversion complete.";
			} else {
				msg = "Conversion failed:"+convert.getReason();
			}
			return msg;
		}
		
	}
}
