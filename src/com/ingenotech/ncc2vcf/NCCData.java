/*
 * $Id$
 * Created on 3 Apr 2008
 */

package com.ingenotech.ncc2vcf;

import java.util.TreeMap;

/**
 * Fields for a row of NCC data
 */
@SuppressWarnings("serial")
public class NCCData extends TreeMap<NCCKey, String> {

	public NCCData() {
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (NCCKey key : keySet()) {
			sb.append(key);
			sb.append("=>");
			sb.append(get(key));
			sb.append("; ");
		}
		return sb.toString();
	}
}
