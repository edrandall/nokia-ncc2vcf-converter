/*
 * $Id$
 * Created on 4 Apr 2008
 */

package com.ingenotech.ncc2vcf;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import com.ingenotech.ncc2vcf.gui.GUI;

/**
 * @author Ed
 */
public class Convert implements Runnable {
	

	public static final int	SUCCESS = 0;
	public static final int	FAILED	= 1;

	private File			nccFile;
	private File			vcfDir;
	
	private volatile int	status;
	private Throwable		reason;
	
	
	public Convert(File nccFile, 
				   File	vcfDir) {
		this.nccFile = nccFile;
		this.vcfDir = vcfDir;
	}
	
	public int getStatus() {
		return this.status;
	}
	
	public Throwable getReason() {
		return this.reason;
	}
	
	public void run() {
		
		try {
			System.out.println("Converting " +nccFile.getCanonicalPath() +" to "+vcfDir.getCanonicalPath());

			NCCReader reader = new NCCReader(new FileInputStream( nccFile ));
			List<NCCData> nccList = reader.readNCC();
			reader.close();

			DataMapper mapper = new DataMapper();
			if (vcfDir.exists()) {
				if (!vcfDir.isDirectory())
					throw new IOException("Need to create a directory for VCF files called "+vcfDir.getAbsolutePath()+" but a file of that name already exsist.");
			} else {
				vcfDir.mkdirs();
			}

			int nvcf = 0;
			for (NCCData ncc : nccList) {
				VCFData vcf = mapper.map(ncc);
				if (vcf != null) {
					File vf = vcf.createFilename(vcfDir);
					vcf.write(vf);
					++nvcf;
					//System.out.println("Wrote VCF record:"+vcf.getName()+" to file:"+vf);
				}
			}

			System.out.println("Converted "+nvcf+" records.");
			this.status = SUCCESS;
		
		} catch (IOException ex) {
			this.status = FAILED;
			this.reason = ex;
		}

	}

	public static final String USAGE = 
			"\nNCC2VCF: Convert an .ncc-format PhoneBook from an older Nokia phone to .vcf format"
			+"\n         these can then be transferred to your new phone using PC Suite."
			+"\nUsage:"
			+"\njava -jar ncc2vcf.jar [-text] [nccfile] [vcfdir]"
			+"\n-text    Run in text-only non-GUI mode"
			+"\nnccfile: The .ncc-format phonebook file to convert"
			+"\n         default: PhoneBook.ncc in the current directory"
			+"\nvcfdir:  Directory to contain new .vcf files, one per contact"
			+"\n         default: a new directory named vcf is created below the current directory"
			;

	
	public static void main(String[] args) {
		try {
			boolean noGUI = false;
			File nccFile = null;
			File vcfDir = null;

			for (int i=0; i<args.length; i++) {
				if ("-text".equalsIgnoreCase(args[i])) {
					noGUI = true;
					
				} else if (nccFile == null) {
					nccFile = new File(args[i]);
					
				} else if (vcfDir == null) {
					vcfDir = new File(args[i]);
				}
			}
			
			if (nccFile == null)
				nccFile = new File("PhoneBook.ncc");
			if (vcfDir == null) 
				vcfDir = new File("vcf");

			if (noGUI) {
				Convert c = new Convert(nccFile, vcfDir);
				c.run();
				
			} else {
				new GUI();
			}
			
		} catch (Exception ex) {
			System.err.println("Error:");
			ex.printStackTrace(System.err);
			System.err.println(USAGE);
		}

	}

}
