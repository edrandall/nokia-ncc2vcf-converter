/*
 * $Id$
 * Created on 3 Apr 2008
 */

package com.ingenotech.ncc2vcf;

public enum VCFKey {

	BEGIN				("BEGIN", 	"VCARD"),	// VCARD
	VERSION				("VERSION", "2.1"),		// 2.1

	NAME				("N"),					// lastname;firstname;middlename
	FULLNAME			("FN"),					// firstname middlename lastname
	BIRTHDAY			("BDAY"),				// yyyymmdd
	NOTE				("NOTE"),

	TEL					("TEL"),
	TEL_CELL			("TEL;CELL"),
	TEL_FAX				("TEL;FAX"),
	TEL_VIDEO			("TEL;VIDEO"),
	EMAIL 				("EMAIL"),
	URL					("URL"),

	TEL_HOME			("TEL;HOME"),
	TEL_CELL_HOME		("TEL;CELL;HOME"),
	TEL_FAX_HOME		("TEL;FAX;HOME"),
	TEL_VIDEO_HOME		("TEL;VIDEO;HOME"),
	EMAIL_HOME			("EMAIL;HOME"),
	URL_HOME			("URL;HOME"),

	TEL_WORK			("TEL;WORK"),
	TEL_CELL_WORK		("TEL;CELL;WORK"),
	TEL_FAX_WORK		("TEL;FAX;WORK"),
	TEL_VIDEO_WORK		("TEL;VIDEO;WORK"),
	EMAIL_WORK 			("EMAIL;WORK"),
	URL_WORK			("URL;WORK"),
	TITLE				("TITLE"),					// business title
	
	END					("END", "VCARD");			// VCARD
	
	
	private final String	tag;
	private final String	value;
	
	VCFKey(String id) {
		this(id, null);
	}
	
	VCFKey(String id, String value) {
		this.tag = id;
		this.value = value;
	}
	
	public String	getTag() {
		return this.tag;
	}

	public String 	toString() {
		return toString(null);
	}
	
	public String	toString(String v) {
		StringBuilder sb = new StringBuilder();
		sb.append(tag);
		sb.append(":");
		if (this.value != null)
			sb.append(this.value);
		else if (v != null)
			sb.append(v);
		return sb.toString();
	}
}
