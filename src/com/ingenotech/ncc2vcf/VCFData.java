/*
 * $Id$
 * Created on 3 Apr 2008
 */

package com.ingenotech.ncc2vcf;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.TreeMap;

/**
 * Contains a VCF address card
 */
@SuppressWarnings("serial")
public class VCFData extends TreeMap<VCFKey, String> {
	
	public String getName() {
		String name = get(VCFKey.FULLNAME);
		if (name == null)
			name = get(VCFKey.NAME);
		if (name == null)
			name = "UNKNOWN";
		return name;
	}
	
	/**
	 * Create a unique filename for the VCard in the given dir.
	 */
	public File createFilename(File dir) {
		String name = getName();
		StringBuilder sb = new StringBuilder( name );
		for (int i=0; i<sb.length(); i++) {
			if (!Character.isLetterOrDigit( sb.charAt(i) ) ) {
				sb.setCharAt(i, '_');
			}
		}
		File fn = new File(dir, sb.toString()+".vcf");
		int n = 0;
		while (fn.exists()) {
			++n;
			fn = new File(dir, sb.toString()+"["+n+"].vcf");
		}
		return fn;
	}
	
	
	public void write(File file) throws IOException {
		FileWriter fw = new FileWriter(file);
		PrintWriter pw = new PrintWriter(fw);
		toString(pw);
		pw.close();
	}
	
	
	public void toString(PrintWriter pw) {
		pw.println(VCFKey.BEGIN.toString());
		pw.println(VCFKey.VERSION.toString());
		for (VCFKey key : keySet()) {
			pw.println(key.toString( get(key) ) );
		}
		pw.println(VCFKey.END.toString());
	}
	
	public String	toString() {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		toString(pw);
		pw.close();
		return sw.toString();
	}
}
