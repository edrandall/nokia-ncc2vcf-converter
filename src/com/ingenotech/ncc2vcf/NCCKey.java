/*
 * $Id$
 * Created on 3 Apr 2008
 */

package com.ingenotech.ncc2vcf;


/**
 * Key for an item of NCC data.
 */
public class NCCKey implements Comparable<Object> {

	public static final NCCKey	N200			= new NCCKey(200);	// CONTACT
	public static final NCCKey	N202			= new NCCKey(202);	// FN
	public static final NCCKey	N204			= new NCCKey(204);	// NOTE
	public static final NCCKey	N205			= new NCCKey(205);	// EMAIL
	public static final NCCKey	N208			= new NCCKey(208);	// TEL 002
	public static final NCCKey	N209			= new NCCKey(209);	// TEL;HOME 004
	public static final NCCKey	N210			= new NCCKey(210);	// TEL;CELL 001
	public static final NCCKey	N213			= new NCCKey(213);	// TEL;CELL;WORK 005
	public static final NCCKey	N219			= new NCCKey(219);	// TEL;CELL;HOME 003
	
	private Integer	key;
	
	
	public NCCKey(String k) throws IllegalArgumentException {
		try {
			setKey( new Integer(k) );
			
		} catch (NumberFormatException ex) {
			throw new IllegalArgumentException("NCCKey:"+k+" must be 3-digit numeric");
		}
	}
	
	public NCCKey(Integer k) throws IllegalArgumentException {
		setKey(k);
	}
	
	private void setKey(Integer k) {
		if (k < 100 || k > 999) {
			throw new IllegalArgumentException("NCCKey:"+k+" must be a 3-digit number");
		}
		this.key = k;
	}

	public String toString() {
		return String.valueOf(key);
	}
	
	
	public int hashCode() {
		return key.hashCode();
	}

	
	public boolean equals(Object other) {
		if (other == this)
			return true;
		if (!(other instanceof NCCKey))
			return false;
		NCCKey no = (NCCKey)other;
		return (key.equals(no.key));
	}


	public int compareTo(Object other) {
		if (other == this)
			return 0;
		if (!(other instanceof NCCKey))
			return -1;
		NCCKey no = (NCCKey)other;
		return (key.compareTo(no.key));
	}
}
