/*
 * $Id$
 * Created on 3 Apr 2008
 */

package com.ingenotech.ncc2vcf;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Read data from a .ncc file
 */
public class NCCReader  {
	
	private BufferedReader	reader;
	
	public NCCReader(InputStream instr) {
		try {
			this.reader = new BufferedReader( new InputStreamReader( instr, "UTF-16" ) );
			
		} catch (UnsupportedEncodingException ex) {
		}
	}
	
	public void close() throws IOException {
		reader.close();
	}
	
	public List<NCCData>	readNCC() throws IOException {
		List<NCCData> li = new LinkedList<NCCData>();

		NCCData ncc;
		while ((ncc = readNCCData()) != null) {
			li.add(ncc);
		}
		
		return li;
	}
	
	public NCCData	readNCCData() throws IOException {
		String line = reader.readLine();
		if (line == null)
			return null;
		
		NCCData data = new NCCData();
		StringTokenizer st = new StringTokenizer(line, "\t");
		while (st.hasMoreTokens()) { 
			String k = st.nextToken();
			if (!st.hasMoreTokens())
				break;
			String v = st.nextToken();
			
			try {
				NCCKey key = new NCCKey(k);
				data.put(key, v);

			} catch (IllegalArgumentException ex) {
			}
		}
		
		return data;
	}
}
