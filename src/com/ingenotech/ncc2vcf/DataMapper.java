/*
 * $Id$
 * Created on 3 Apr 2008
 */

package com.ingenotech.ncc2vcf;

import java.util.HashMap;
import java.util.Map;

public class DataMapper {
	

	
	private Map<NCCKey, VCFKey> 	map;
	
	public DataMapper() {
		this.map = new HashMap<NCCKey, VCFKey>();
		map.put(NCCKey.N202, VCFKey.FULLNAME);
		map.put(NCCKey.N204, VCFKey.NOTE);
		map.put(NCCKey.N205, VCFKey.EMAIL);
		map.put(NCCKey.N208, VCFKey.TEL);
		map.put(NCCKey.N209, VCFKey.TEL_HOME);
		map.put(NCCKey.N210, VCFKey.TEL_CELL);
		map.put(NCCKey.N213, VCFKey.TEL_CELL_WORK);
		map.put(NCCKey.N219, VCFKey.TEL_CELL_HOME);
	}
	
	public VCFData	map(NCCData ncc) {

		if (!ncc.containsKey(NCCKey.N200)) 
			return null;
		
		VCFData vcf = new VCFData();
		
		for (NCCKey nccKey : ncc.keySet()) {
			VCFKey vcfKey = map.get(nccKey);
			if (vcfKey == null) 
				continue;

			String value = ncc.get(nccKey);
			if (value == null)
				continue;
			
			value = value.trim();
			if (value.length() < 1)
				continue;
			
			vcf.put(vcfKey, value);
				
			if (vcfKey == VCFKey.FULLNAME) {
				// map to N also
				String[] vs = value.split(" ");
				switch (vs.length) {
					case 0:
						break;
					case 1:
						vcf.put(VCFKey.NAME, vs[0]);
						break;
					case 2:
						vcf.put(VCFKey.NAME, vs[1]+";"+vs[0]);
						break;
					case 3:
						vcf.put(VCFKey.NAME, vs[2]+";"+vs[0]+";"+vs[1]);
						break;
					default: // > 3
						{
							StringBuilder sb = new StringBuilder();
							for (int i=2; i<vs.length; i++) {
								if (i > 2)
									sb.append(' ');
								sb.append(vs[i]);
							}
							sb.append(';');
							sb.append(vs[0]);
							sb.append(';');
							sb.append(vs[1]);
							vcf.put(VCFKey.NAME, sb.toString());
						}
						break;
				}
			}
		}
		
		return vcf;
	}
}
